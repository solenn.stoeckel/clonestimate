# ClonEstiMate
version 1.02, March 2018

## Description

This is command-line software of the bayesian method using changes in genotype frequencies locus by locus to estimate rates of clonality in finite populations that mutate using two temporal genotyped samples.

---

To cite this work and use the script, please mention: 

**Becheler Ronan, Masson Jean-Pierre, Arnaud-Haond Sophie, Halkett Fabien, Mariette Stéphanie, Guillemin Maire-Laure, Valéro Myriam, Destombe Christophe, Stoeckel Solenn (2017). ClonEstiMate, a Bayesian method for quantifying rates of clonality of populations genotyped at two‐time steps. Molecular Ecology Resources, 17, e251–e267. https://doi.org/10.1111/1755-
0998.12698**

---

CC-BY-NC-SA license, version 4: Solenn Stoeckel, INRA, 2016.

## Update
This method is now generalized to any polyploidy and available in a user-friendly sofware with graphical interface GenAPoPop. Please see https://forgemia.inra.fr/solenn.stoeckel/genapopop1.0 


